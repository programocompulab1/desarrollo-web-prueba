from django.db import models

class Plato(models.Model):
    nombre = models.CharField(max_length = 100)
    precio = models.IntegerField(default = 15)
    imagen = models.ImageField(null=True, upload_to='galeria')
