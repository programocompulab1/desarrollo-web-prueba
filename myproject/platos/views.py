from django.shortcuts import render
from .models import Plato
from django.core.mail import send_mail
from django.conf import settings

def index(request):
    plato_lista = Plato.objects.order_by('precio')
    contexto = {'plato_lista': plato_lista}
    return render(request, 'platos/index.html', contexto)

def contacto(request):
    if request.method=="POST":
        subject = 'Contacto Pollito con Papas'
        message = request.POST["mensaje"] + '. Enviado por ' + request.POST["nombre"] + ' del Email: ' + request.POST["email"]
        email_from = settings.EMAIL_HOST_USER
        recipient_list = ["sebaskpop123@gmail.com"]
        send_mail(subject, message, email_from, recipient_list)
        plato_lista = Plato.objects.order_by('precio')
        contexto = {'plato_lista': plato_lista}
        return render(request, 'platos/index.html', contexto)
    return render(request, 'platos/contacto.html')